# Dillinger

Un poco de documentación de Ansible
![Logo]
Si quieres aprender Ansible, necesitas la [documentación]
Aquí puedes obtener dicha [documentación]

[Logo]: https://upload.wikimedia.org/wikipedia/commons/0/05/Ansible_Logo.png
[documentación]:https://doc.ansible.com/ansible/latest/index.html

### Comandos Ad-Hoc

Para ejecutar el comando ad-hoc, se debe escribir el comando `ansible` se debe utilizar un grupo seguido del módulo, por ejemplo:

```sh
$ ansible grupo1 -m ping
```

### Correr Playbooks

Para ejecutar un playbook corra el siguiente comando
```sh
$ ansible-playbook miplay.yml
```

Este es mi playbook con nombre miplay.yml

```yml
- host: grupo1
  tasks:
    - name: Instalar git
        yum:
            name: git
            state: latest
```


> Nota: Debes diagnosticar la conectividad hacia los nodos remotos con el módulo `ping`
Esto es de vital importancia

Aquí veremos un poco de tablas

### Módulos de Ansible

Nombre | Documentación
|---|---
Copy | [Ir](https://docs.ansible.com/ansible/latest/modules/copy_module.html)
yum | [Ir](https://docs.ansible.com/ansible/latest/modules/yum_module.html)







declare @fecha varchar(MAX)
declare @archivo varchar(MAX)
set @fecha = SUBSTRING(Convert(Varchar(10), GetDate(),105),1,2)+SUBSTRING(Convert(Varchar(10), GetDate(),105),4,2)+SUBSTRING(Convert(Varchar(10), GetDate(),105),7,4)+'_'+SUBSTRING(CONVERT(Varchar(10), GetDate(),108),1,2)+SUBSTRING(CONVERT(Varchar(10), GetDate(),108),4,2)
set @archivo ='D:\Respaldos\2020\Enero\ERP2020_'+@fecha+'.bak'
BACKUP DATABASE ERP2020_Company
TO DISK = @archivo
   WITH FORMAT,
      MEDIANAME = 'D_SQLServerBackups',
      NAME = 'Full Backup of ERP2020_Company';
GO
